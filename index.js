const express = require('express');
const api = express();
const mongoose = require('mongoose');
const cors = require('cors');

api.use(cors());

require('dotenv').config();

const connectionString = process.env.DB_CONNECTION_STRING;
mongoose.connect(connectionString, {
	useNewUrlParser: true,
	useUnifiedTopology: true,
	useFindAndModify: false
});
mongoose.connection.once('open', () => console.log('Database connection successful!'));

api.use(express.json());
api.use(express.urlencoded({ extended: true }));

const userRoutes = require('./routes/user');
const courseRoutes = require('./routes/course');

api.use('/api/users', userRoutes);
api.use('/api/courses', courseRoutes);

api.listen(process.env.PORT, () => {
    console.log(`Listening on port ${ process.env.PORT }.`);
});